package com.example.mayarelmohr.mpp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
public class HomeActivity extends AppCompatActivity {

    private ListView mDrawerList;

    private DrawerLayout mDrawerLayout;

    public static String password = "12345";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.navList);
        addDrawerItems();

        final Button student = (Button) findViewById(R.id.student);
        final Button teacher = (Button) findViewById(R.id.prof);
        final ImageButton studentImage = (ImageButton) findViewById(R.id.haticon);
        final ImageButton profImage = (ImageButton) findViewById(R.id.profficon);



        studentImage.setOnClickListener(onClickListener1);
        student.setOnClickListener(onClickListener1);
        profImage.setOnClickListener(onClickListener2);
        teacher.setOnClickListener(onClickListener2);



    }


    private void addDrawerItems() {
        String[] osArray = { "Home", "Profile", "About"};


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> newAdapterView, View view, int position, long id) {
                selectItem(position);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

    }
    public void selectItem(int position) {
        Intent intent = null;
        switch(position) {
            case 0:
                intent = new Intent(this, HomeActivity.class);
                break;
            case 1:
                intent = new Intent(this, profile.class);
                break;

            case 2:
                intent = new Intent(this, about_us.class);
                break;

            default :
                break;
        }

        startActivity(intent);
    }

    private View.OnClickListener onClickListener1 = new View.OnClickListener() {
        @Override

        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
        }

    };

    private View.OnClickListener onClickListener2 = new View.OnClickListener() {
        @Override

        public void onClick(View v) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);


            alertDialog.setTitle("Professor");
            alertDialog.setMessage("Please enter the password");
            final EditText input = new EditText(HomeActivity.this);
            LinearLayout.LayoutParams lin = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lin);
            alertDialog.setView(input);
            input.setInputType(0x00000081);

            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences cachedVersion = getSharedPreferences("cached_version", 0);
                            String pass = cachedVersion.getString("password",password);
                            if (input.getText().toString().equals(pass)) {
                                Intent i = new Intent(getApplicationContext(), TeacherActivity.class);
                                startActivity(i);
                            } else {
                                error();
                            }


                            dialog.cancel();
                        }
                    });
            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });


            alertDialog.show();
        }

    };

    private void error() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(HomeActivity.this);
        builder1.setMessage("Wrong Password!");
        builder1.setCancelable(true);
        builder1.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}