package com.example.mayarelmohr.mpp;

import com.example.mayarelmohr.mpp.MainActivity;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mayarelmohr.mpp.Time.Day;
import com.example.mayarelmohr.mpp.utilities.EncryptDecryptStringWithDES;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.HashMap;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;




public class TeacherActivity extends Activity implements EasyPermissions.PermissionCallbacks {
    private ListView mDrawerList;

    private DrawerLayout mDrawerLayout;
    //  ToggleButton tglRead;;
    NfcAdapter nfcAdapter;
    //EditText content;
    // EditText write;
    boolean clicked;
    static String message = "";
    String initials = "";
    static Button sat1, sat2, sat3, sat4, sat5, sun1, sun2, sun3, sun4, sun5, wed1, wed2, thurs5;
    //static Button[] firstSlot = {sat1, sun1, wed1};
    static Button[] firstSlot;
    static Button[] secondSlot = {sat2, sun2, wed2};
    static Button[] thirdSlot = {sat3, sun3};
    static Button[] fourthSlot = {sat4, sun4};
    static Button[] fifthSlot = {sat5, sun5, thurs5};
    static HashMap<Integer, String> map = new HashMap<Integer, String>();
    static String time = "";
    static ArrayList<String> reservedSlots = new ArrayList<String>();
    static Intent intento;
    static String sherine;

    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {CalendarScopes.CALENDAR};

    public ArrayList<String> googleCalenderData = new ArrayList<>();

    // [days][slots][fragment]
    static String fragments[][][] = new String[6][5][3];

    void initFragment() {
        for (int i = 0; i < fragments.length; i++) {
            for (int j = 0; j < fragments[i].length; j++) {
                for (int k = 0; k < fragments[i][j].length; k++) {
                    fragments[i][j][k] = " ";
                }
            }
        }
    }

    void deserialize(String msg) {
        String[] days = msg.split("_");
        days = Arrays.copyOfRange(days, 1, days.length);
        for (int i = 0; i < days.length - 1; i++) {
            String[] slots = days[i].split("/");
            slots = Arrays.copyOfRange(slots, 1, slots.length);
            for (int j = 0; j < slots.length - 1; j++) {
                String[] partitions = slots[j].split("-");
                //           partitions = Arrays.copyOfRange(partitions, 1, partitions.length);
                for (int k = 0; k < partitions.length - 1; k++) {
                    fragments[i][j][k] = partitions[k + 1];
                }
            }
        }
    }

    String serialize() {
        String result = "";
        for (int i = 0; i < fragments.length; i++) {
            result += "_";
            for (int j = 0; j < fragments[i].length; j++) {
                result += "/";
                for (int k = 0; k < fragments[i][j].length; k++) {
                    result += "-";
                    result += fragments[i][j][k];
                }
            }

        }

        return result;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.navList);
        addDrawerItems();
        String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
        TextView text = (TextView) findViewById(R.id.datee);
        text.setText(currentDateTimeString);
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 5; j++) {
                String key = "b" + i + j;
                Button btn = (Button) findViewById(getResources().getIdentifier(key, "id", getPackageName()));
                map.put(btn.getId(), i + " " + j);
                btn.setClickable(true);
                btn.setOnClickListener(onClickListener);

            }
        }


        Button bt = (Button) findViewById(R.id.new_schedule);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), OfficeHoursActivity.class);
                startActivity(i);
            }
        });

        // Initialize credentials and service object.
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Fetching From Google Calendar ...");
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            Toast.makeText(this, "NFC available", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "NFC Not Found", Toast.LENGTH_LONG).show();
            finish();
        }


    }

    private void addDrawerItems() {
        String[] osArray = { "Home", "Profile", "About"};


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> newAdapterView, View view, int position, long id) {
                selectItem(position);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

    }
    public void selectItem(int position) {
        Intent intent = null;
        switch(position) {
            case 0:
                intent = new Intent(this, HomeActivity.class);
                break;
            case 1:
                intent = new Intent(this, profile.class);
                break;

            case 2:
                intent = new Intent(this, about_us.class);
                break;

            default :
                break;
        }

        startActivity(intent);
    }

    private View.OnClickListener changePassword = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(TeacherActivity.this);
            alertDialog.setTitle("Change Password");
            LinearLayout ret = new LinearLayout(TeacherActivity.this);
            ret.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            ret.setOrientation(LinearLayout.VERTICAL);

            final TextView ax = new TextView(TeacherActivity.this);
            ax.setText("New Password");
            //ax.setGravity(Gravity.CENTER);
            final EditText input = new EditText(TeacherActivity.this);
            ret.setOrientation(LinearLayout.VERTICAL);
            ret.addView(ax);
            ret.addView(input);
            alertDialog.setView(ret);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (input.getText().toString().equals("")) {
                                errorPassword();
                            }
                            SharedPreferences cachedVersion = getSharedPreferences("cached_version", 0);
                            SharedPreferences.Editor editor = cachedVersion.edit();
                            editor.putString("password", input.getText().toString());
                            editor.commit();
                            HomeActivity.password = input.getText().toString();
                            dialog.cancel();
                        }
                    });
            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });

            alertDialog.show();
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, TeacherActivity.class);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        IntentFilter[] intentFilter = new IntentFilter[]{};
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilter, null);

    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        intento = intent;
        if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Toast.makeText(TeacherActivity.this, "NFC intent", Toast.LENGTH_LONG).show();
            Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (parcelables != null && parcelables.length > 0) {
                readTextFromTag((NdefMessage) parcelables[0]);
                Log.i("Contenttt <<<<< ", message);
            }
        }
    }

    private NdefRecord createTextRecord(String content) {
        try {
            byte[] language;
            language = Locale.getDefault().getLanguage().getBytes("UTF-8");
            final byte[] text = content.getBytes("UTF-8");
            final int languageSize = language.length;
            final int textLength = text.length;
            final ByteArrayOutputStream payload = new ByteArrayOutputStream(1 + languageSize + textLength);
            payload.write((byte) (languageSize & 0x1F));
            payload.write(language, 0, languageSize);
            payload.write(text, 0, textLength);
            return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload.toByteArray());
        } catch (UnsupportedEncodingException e) {
            Log.e("createTextRecord", e.getMessage());
        }
        return null;
    }

    private boolean isWellFormatted(String cont) {
        String[] split = cont.split("@");
        if (cont.isEmpty() || split.length != 2 || split.length != 1)
            return false;
        else
            return true;
    }

    public void readTextFromTag(NdefMessage ndefMessage) {
        NdefRecord[] ndefRecords = ndefMessage.getRecords();
        if (ndefRecords != null && ndefRecords.length > 0) {
            NdefRecord ndefRecord = ndefRecords[0];
            String tagContent = getTextFromNdefRecord(ndefRecord).trim();
            tagContent = EncryptDecryptStringWithDES.performDecryption(tagContent);
            if (!message.isEmpty())
                tagContent = message;
            if (isWellFormatted(tagContent)) {
                SharedPreferences cachedVersion = getSharedPreferences("cached_version", 0);
                tagContent = cachedVersion.getString("cached_version", "");
            }
            String[] ax = tagContent.split("@");
            sherine = ax[0];
            if (ax.length == 1) {
                tagContent = "";
            } else {
                tagContent = ax[1];
            }
            Log.i("Content_on tag is", tagContent);
            deserialize(tagContent);
            for (int i = 0; i < fragments.length; i++) {
                for (int j = 0; j < fragments[i].length; j++) {
                    String ini = "";
                    Button m = (Button) findViewById(getResources().getIdentifier("b" + i + j, "id", getPackageName()));
                    for (int k = 0; k < fragments[i][j].length; k++) {
                        if (fragments[i][j][k] != null && fragments[i][j][k].length() != 0 && !fragments[i][j][k].equals("null")) {
                            ini += getInitials(fragments[i][j][k].toString()) + "|";

                            if((fragments[i][j][k].toString().charAt(fragments[i][j][k].length() - 1)) == '1') {
                                Log.e("HAVE_INTERNET"," true");
                            }
                            else {
                                String [] startEndTimes = MainActivity.generateSlotDateTime(i + "", j + "", k +"");
                                String [] splits = fragments[i][j][k].split("&");
                                Log.e("KOLO: ", splits[2]);

//
                                googleCalenderData.add(splits[2]);
                                googleCalenderData.add(startEndTimes[0]);
                                googleCalenderData.add(startEndTimes[1]);
                                googleCalenderData.add(splits[3]);
                                googleCalenderData.add("0");
                                getResultsFromApi();
                                Log.e("HAVE_INTERNET"," false");
                            }

                        }
                    }
                    if (m != null) {
                        m.setTextSize(11);
                        m.setText(ini);

                    }
                }
            }
        } else {
            Toast.makeText(this, "No NDEF records found", Toast.LENGTH_LONG).show();
        }

    }

    public String getTextFromNdefRecord(NdefRecord ndefRecord) {
        String tagContent = null;
        try {
            byte[] payload = ndefRecord.getPayload();
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
            int languageSize = payload[0] & 0063;
            tagContent = new String(payload, languageSize + 1,
                    payload.length - languageSize - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("getTextFromNdefRecord", e.getMessage(), e);
        }
        return tagContent;
    }

    private NdefMessage createNdefMessage(String content) {
        NdefRecord ndefRecord = createTextRecord(content);
        NdefMessage ndefMessage = new NdefMessage(new NdefRecord[]{ndefRecord});
        return ndefMessage;
    }


    private void formatTag(Tag tag, NdefMessage ndefMessage) {
        try {
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);
            if (ndefFormatable == null) {
                Toast.makeText(this, "Tag is not ndef formatable!", Toast.LENGTH_SHORT).show();
                return;
            }
            ndefFormatable.connect();
            ndefFormatable.format(ndefMessage);
            ndefFormatable.close();
            Toast.makeText(this, "Tag writen!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("formatTag", e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String getInitials(String s) {
        String[] spInitials = s.split(" ");
        String initials = "";
        for (String s1 : spInitials)
            initials += s1.charAt(0);

        return initials;
    }



    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override

        public void onClick(View v) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(TeacherActivity.this);
            alertDialog.setTitle("Office hours");
            LinearLayout ret = new LinearLayout(TeacherActivity.this);
            ret.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            ret.setOrientation(LinearLayout.VERTICAL);
            final Button x = (Button) findViewById(v.getId());
            String day_slot = map.get(x.getId());
            final int day = Integer.parseInt(day_slot.split(" ")[0]);
            final int slot = Integer.parseInt(day_slot.split(" ")[1]);


            String[] names = fragments[day][slot];
            String[] nn = {"1st partition", "2nd partition", "3rd partition"};
            for (int i = 0; i < names.length; i++) {
                if (names[i] != null && !names[i].equals("null")) {
                    final TextView ax = new TextView(TeacherActivity.this);
                    ax.setText(nn[i]);
                    ax.setGravity(Gravity.CENTER);
                    ret.addView(ax);
                    final TextView ay = new TextView(TeacherActivity.this);
                    ay.setText("Name: " + names[i].split("&")[0]);
                    ay.setGravity(Gravity.CENTER);
                    final TextView az = new TextView(TeacherActivity.this);
                    if (names[i].split("&").length > 1) {
                        String mg = names[i].split("&")[1];
                        az.setText("Message: " + mg);
                    }
                    az.setGravity(Gravity.CENTER);
                    ret.addView(ay);
                    ret.addView(az);
                }
            }
            //  ret.addView(a);
            alertDialog.setView(ret);
            alertDialog.setNegativeButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            alertDialog.show();
            //    alertDialog.setMessage("Please write down your name");

        }

    };


//    private void performWriteAction(Intent intent, String msg) {
//        Tag tag = intento.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//        msg = sherine + "@" + msg;
//        NdefMessage ndefMessage = createNdefMessage(msg);
//        Toast.makeText(TeacherActivity.this, "Writing Message: " + msg, Toast.LENGTH_LONG).show();
//        writeNdefMessage(tag, ndefMessage);
//    }

    void performReadAction(Intent intent) {
        if (intento.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Parcelable[] parcelables = intento.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (parcelables != null && parcelables.length > 0) {
                readTextFromTag((NdefMessage) parcelables[0]);
            }
        }
    }

    private void error() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(TeacherActivity.this);
        builder1.setMessage("Please select a time slot");
        builder1.setCancelable(true);
        builder1.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void errorPassword() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(TeacherActivity.this);
        builder1.setMessage("Please don't choose an empty password");
        builder1.setCancelable(true);
        builder1.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    /* GOOGLE CALENDAR API */
    /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private void getResultsFromApi() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (!isDeviceOnline()) {
            Toast.makeText(getApplicationContext(), "No network connection available.", Toast.LENGTH_SHORT).show();
        } else {
            new MakeRequestTask(mCredential).execute();
        }
    }

    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     *
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode  code indicating the result of the incoming
     *                    activity result.
     * @param data        Intent (containing result data) returned by incoming
     *                    activity result.
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    Toast.makeText(getApplicationContext(), "This app requires Google Play Services. Please install " +
                            "Google Play Services on your device and relaunch this app.", Toast.LENGTH_SHORT).show();
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;
        }
    }

    /**
     * Respond to requests for permissions at runtime for API 23 and above.
     *
     * @param requestCode  The request code passed in
     *                     requestPermissions(android.app.Activity, String, int, String[])
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either PERMISSION_GRANTED or PERMISSION_DENIED. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    /**
     * Callback for when a permission is granted using the EasyPermissions
     * library.
     *
     * @param requestCode The request code associated with the requested
     *                    permission
     * @param list        The requested permission list. Never null.
     */
    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }

    /**
     * Callback for when a permission is denied using the EasyPermissions
     * library.
     *
     * @param requestCode The request code associated with the requested
     *                    permission
     * @param list        The requested permission list. Never null.
     */
    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

    /**
     * Checks whether the device currently has a network connection.
     *
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     *                             Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                TeacherActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    /**
     * An asynchronous task that handles the Google Calendar API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
        private com.google.api.services.calendar.Calendar mService = null;
        private Exception mLastError = null;

        MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Google Calendar API Android Quickstart")
                    .build();
        }

        /**
         * Background task to call Google Calendar API.
         *
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of the next 10 events from the primary calendar.
         *
         * @return List of Strings describing returned events.
         * @throws IOException
         */
        public List<String> getDataFromApi() throws IOException {
            // List the next 10 events from the primary calendar.
            DateTime now = new DateTime(System.currentTimeMillis());
            List<String> eventStrings = new ArrayList<String>();
            Events events = mService.events().list("primary")
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();

            MainActivity.createEvent(mService, googleCalenderData.get(0), googleCalenderData.get(1), googleCalenderData.get(2), googleCalenderData.get(3), googleCalenderData.get(4));
            List<Event> items = events.getItems();

            for (Event event : items) {
                DateTime start = event.getStart().getDateTime();
                if (start == null) {
                    start = event.getStart().getDate();
                }
                eventStrings.add(start.toString());
            }
            return eventStrings;
        }


        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @Override
        protected void onPostExecute(List<String> output) {
            mProgress.hide();
            Log.e("RES", "onPostExecute: " + output.toString());
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            MainActivity.REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(getApplicationContext(), "The following error occurred:\n"
                            + mLastError.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Request cancelled.", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
