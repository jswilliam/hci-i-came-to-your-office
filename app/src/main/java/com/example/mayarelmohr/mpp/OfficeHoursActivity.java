package com.example.mayarelmohr.mpp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mayarelmohr.mpp.utilities.EncryptDecryptStringWithDES;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class OfficeHoursActivity extends Activity {
    NfcAdapter nfcAdapter;
    Button sat1, sat2, sat3, sat4, sat5, sun1, sun2, sun3, sun4, sun5, mon1, mon2, mon3, mon4, mon5, tue1, tue2, tue4,
            tue3, tue5, wed1, wed2, wed3, wed4, wed5, thurs1, thurs2, thurs3, thurs4, thurs5;
    Button[] calendar;
    boolean clicked = false;
    String slots = "";
    static HashMap<Integer, String> map = new HashMap<Integer, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_office_hours);
        String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
        TextView text = (TextView) findViewById(R.id.datee);
        text.setText(currentDateTimeString);
        init();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            Toast.makeText(this, "NFC available", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "NFC Not found", Toast.LENGTH_LONG).show();
            finish();
        }

        final Button write = (Button) findViewById(R.id.Write);
        write.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clicked = true;
                Log.i("Message is ", " clicked okay");
            }
        });
    }


    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, OfficeHoursActivity.class);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        IntentFilter[] intentFilter = new IntentFilter[]{};
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilter, null);

    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Toast.makeText(this, "NFC intent", Toast.LENGTH_LONG).show();
            if (clicked) {
                // Perform action on click
                Tag tag1 = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                //  String text_to_write = SaturdayFirst.getText()+ "-" + SaturdaySecond.getText();
                slots += "@";
                String encrypted = EncryptDecryptStringWithDES.performEncryption(slots);
                NdefMessage ndefMessage = createNdefMessage(encrypted);
                Toast.makeText(this, "Message written is " + slots, Toast.LENGTH_LONG).show();
                Log.i("TAG_JOE", "onNewIntent: " + slots);
                writeNdefMessage(tag1, ndefMessage);
                clicked = false;
            }
        }

    }


    private void formatTag(Tag tag, NdefMessage ndefMessage) {
        try {
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);
            if (ndefFormatable == null) {
                Toast.makeText(this, "Tag is not ndef formatable!", Toast.LENGTH_SHORT).show();
                return;
            }
            ndefFormatable.connect();
            ndefFormatable.format(ndefMessage);
            ndefFormatable.close();
            Toast.makeText(this, "Tag writen!", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Log.e("formatTag", e.getMessage());
        }
    }

    private void writeNdefMessage(Tag tag, NdefMessage ndefMessage) {
        try {
            if (tag == null) {
                Toast.makeText(this, "Tag object cannot be null", Toast.LENGTH_SHORT).show();
                return;
            }
            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // format tag with the ndef format and writes the message.
                formatTag(tag, ndefMessage);
            } else {
                ndef.connect();
                if (!ndef.isWritable()) {
                    Toast.makeText(this, "Tag is not writable!", Toast.LENGTH_SHORT).show();
                    ndef.close();
                    return;
                }
                ndef.writeNdefMessage(ndefMessage);
                ndef.close();
                Toast.makeText(this, "Tag written!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e("writeNdefMessage", e.getMessage());
        }
    }

    private NdefRecord createTextRecord(String content) {
        try {
            byte[] language;
            language = Locale.getDefault().getLanguage().getBytes("UTF-8");
            final byte[] text = content.getBytes("UTF-8");
            final int languageSize = language.length;
            final int textLength = text.length;
            final ByteArrayOutputStream payload = new ByteArrayOutputStream(1 + languageSize + textLength);
            payload.write((byte) (languageSize & 0x1F));
            payload.write(language, 0, languageSize);
            payload.write(text, 0, textLength);
            return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload.toByteArray());
        } catch (UnsupportedEncodingException e) {
            Log.e("createTextRecord", e.getMessage());
        }
        return null;
    }

    private NdefMessage createNdefMessage(String content) {
        NdefRecord ndefRecord = createTextRecord(content);
        NdefMessage ndefMessage = new NdefMessage(new NdefRecord[]{ndefRecord});
        return ndefMessage;
    }

    private void init() {
        String names[] = {"00", "01", "02", "03", "04", "10", "11", "12", "13", "14", "20", "21"
                , "22", "23", "24", "30", "31", "32", "33", "34", "40", "41", "42", "43", "44", "50", "51", "52", "53", "54"};

        sat1 = (Button) findViewById(R.id.saturday1st);
        sat2 = (Button) findViewById(R.id.saturday2nd);
        sat3 = (Button) findViewById(R.id.saturday3rd);
        sat4 = (Button) findViewById(R.id.saturday4th);
        sat5 = (Button) findViewById(R.id.saturday5th);
        sun1 = (Button) findViewById(R.id.sunday1st);
        sun2 = (Button) findViewById(R.id.sunday2nd);
        sun3 = (Button) findViewById(R.id.sunday3rd);
        sun4 = (Button) findViewById(R.id.sunday4th);
        sun5 = (Button) findViewById(R.id.sunday5th);
        mon1 = (Button) findViewById(R.id.monday1st);
        mon2 = (Button) findViewById(R.id.monday2nd);
        mon3 = (Button) findViewById(R.id.monday3rd);
        mon4 = (Button) findViewById(R.id.monday4th);
        mon5 = (Button) findViewById(R.id.monday5th);
        tue1 = (Button) findViewById(R.id.tuesday1st);
        tue2 = (Button) findViewById(R.id.tuesday2nd);
        tue3 = (Button) findViewById(R.id.tuesday3rd);
        tue4 = (Button) findViewById(R.id.tuesday4th);
        tue5 = (Button) findViewById(R.id.tuesday5th);
        wed1 = (Button) findViewById(R.id.wednesday1st);
        wed2 = (Button) findViewById(R.id.wednesday2nd);
        wed3 = (Button) findViewById(R.id.wednesday3rd);
        wed4 = (Button) findViewById(R.id.wednesday4th);
        wed5 = (Button) findViewById(R.id.wednesday5th);
        thurs1 = (Button) findViewById(R.id.thursday1st);
        thurs2 = (Button) findViewById(R.id.thursday2nd);
        thurs3 = (Button) findViewById(R.id.thursday3rd);
        thurs4 = (Button) findViewById(R.id.thursday4th);
        thurs5 = (Button) findViewById(R.id.thursday5th);
        calendar = new Button[]{sat1, sat2, sat3, sat4, sat5, sun1, sun2, sun3, sun4, sun5, mon1, mon2, mon3, mon4, mon5, tue1, tue2, tue4,
                tue3, tue5, wed1, wed2, wed3, wed4, wed5, thurs1, thurs2, thurs3, thurs4, thurs5};

        for (int i = 0; i < calendar.length; i++) {
            calendar[i].setTextSize(11);
            calendar[i].setOnClickListener(onClickListener);
            map.put(calendar[i].getId(), names[i]);

        }

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override


        public void onClick(View v) {
            String read = map.get(v.getId());
            v.getBackground().setColorFilter(Color.parseColor("#5FD8B1"), PorterDuff.Mode.MULTIPLY);
            slots += read + "-";
        }


    };
}