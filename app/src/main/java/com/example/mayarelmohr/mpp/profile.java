package com.example.mayarelmohr.mpp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by alia on 12/7/2016.
 */
public class profile extends Activity {

    private ListView mDrawerList;

    private DrawerLayout mDrawerLayout;

    Button saveButton;
    EditText nameEdit;
    EditText idEdit;
    EditText emailEdit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_data);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.navList);
        addDrawerItems();

        saveButton = (Button)findViewById(R.id.save_data);
        nameEdit   = (EditText)findViewById(R.id.nameText);
        idEdit   = (EditText)findViewById(R.id.idText);
        emailEdit   = (EditText)findViewById(R.id.emailText);

        saveButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                       MainActivity.userName = nameEdit.getText().toString();
                        MainActivity.userID = idEdit.getText().toString();
                        MainActivity.userEmail = emailEdit.getText().toString();
                        Toast.makeText(getApplicationContext(), "Profile Data Saved", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void addDrawerItems() {
        String[] osArray = { "Home", "Profile", "About"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> newAdapterView, View view, int position, long id) {
                selectItem(position);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

    }
    public void selectItem(int position) {
        Intent intent = null;
        switch(position) {
            case 0:
                intent = new Intent(this, HomeActivity.class);
                break;
            case 1:
                intent = new Intent(this, profile.class);
                break;

            case 2:
                intent = new Intent(this, about_us.class);
                break;

            default :
                break;
        }

        startActivity(intent);
    }
}
