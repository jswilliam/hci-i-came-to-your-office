package com.example.mayarelmohr.mpp;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mayarelmohr.mpp.utilities.EncryptDecryptStringWithDES;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;
import com.google.api.services.calendar.model.Events;

import org.joda.time.LocalTime;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends Activity implements EasyPermissions.PermissionCallbacks {
    NfcAdapter nfcAdapter;
    private ListView mDrawerList;

    private DrawerLayout mDrawerLayout;
    boolean clicked;
    static String message = "";
    //ADDED NEW
    static String userName = "";
    static String userID = "";
    static String userEmail = "";
    String initials = "";
    static HashMap<Integer, String> map = new HashMap<Integer, String>();
    static String time = "";
    static ArrayList<String> reservedSlots = new ArrayList<String>();
    static Intent intento;
    static String construction = "";
    static int ln;
    private static Cipher ecipher;
    private static Cipher dcipher;

    private static SecretKey key;

    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {CalendarScopes.CALENDAR};

    private String fetchedData = "";

    // [days][slots][fragment]
    static String fragments[][][] = new String[6][5][3];
    // [days][slots][fragment]
    //static String fragmentsGoogle[][][] = new String[6][5][3];

    public ArrayList<String> googleCalenderData = new ArrayList<>();

    void initFragment() {
        for (int i = 0; i < fragments.length; i++) {
            for (int j = 0; j < fragments[i].length; j++) {
                for (int k = 0; k < fragments[i][j].length; k++) {
                    fragments[i][j][k] = " ";
                }
            }
        }
    }

    void deserialize(String msg) {
        String[] days = msg.split("_");
//        if (!msg.isEmpty()) {
        days = Arrays.copyOfRange(days, 1, days.length);
        for (int i = 0; i < days.length - 1; i++) {
            String[] slots = days[i].split("/");
            slots = Arrays.copyOfRange(slots, 1, slots.length);
            for (int j = 0; j < slots.length - 1; j++) {
                String[] partitions = slots[j].split("-");
                for (int k = 0; k < partitions.length - 1; k++) {
                    fragments[i][j][k] = partitions[k + 1];
                    Log.w("NFCFr", "deserialize: " + fragments[i][j][k]);
                }
            }
        }
    }

    String serialize() {
        String result = "";
        for (int i = 0; i < fragments.length; i++) {
            result += "_";
            for (int j = 0; j < fragments[i].length; j++) {
                result += "/";
                for (int k = 0; k < fragments[i][j].length; k++) {
                    result += "-";
                    result += fragments[i][j][k];
                }
            }
        }
        Log.w("NFCFr", "serialize: " + result);
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.navList);
        addDrawerItems();

        String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
        TextView text = (TextView) findViewById(R.id.datee);
        text.setText(currentDateTimeString);
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 5; j++) {
                String key = "b" + i + j;
                Button btn = (Button) findViewById(getResources().getIdentifier(key, "id", getPackageName()));
                map.put(btn.getId(), i + " " + j);
                btn.setClickable(false);
            }
        }
        // Initialize credentials and service object.
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Fetching From Google Calendar ...");
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            Toast.makeText(this, "NFC available", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "NFC Not Found", Toast.LENGTH_LONG).show();
            finish();
        }
        //getResultsFromApi();
    }

    private void addDrawerItems() {
        String[] osArray = {"Home", "Profile", "About"};


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> newAdapterView, View view, int position, long id) {
                selectItem(position);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

    }

    public void selectItem(int position) {
        Intent intent = null;
        switch (position) {
            case 0:
                intent = new Intent(this, HomeActivity.class);
                break;
            case 1:
                intent = new Intent(this, profile.class);
                break;

            case 2:
                intent = new Intent(this, about_us.class);
                break;

            default:
                break;
        }

        startActivity(intent);
    }

    void al3b(String slots) {
        String[] days = slots.split("-");
        for (int i = 0; i < days.length - 1; i++) {
            String id = "b" + days[i];
            Button btn = (Button) findViewById(getResources().getIdentifier(id, "id", getPackageName()));
            if (btn != null) {
                btn.setClickable(true);
                btn.setOnClickListener(onClickListener);
            }
        }
//        Set<Integer> set = map.keySet();
//        for (Integer a : set) {
//            String key = map.get(a);
//            String aKey = key.split(" ")[0] + key.split(" ")[1];
//
//            Button btn = (Button) findViewById(getResources().getIdentifier("b" + aKey.trim(), "id", getPackageName()));
//            Log.i("aaaaa", "b" + aKey.trim());
//
//            if (btn != null && !btn.isClickable()) {
//                Log.i("bbbbb", "b" + aKey.trim());
//                btn.setText("NHJHJA");
//                btn.setTextColor(Color.RED);
//            }
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        IntentFilter[] intentFilter = new IntentFilter[]{};
        if (nfcAdapter != null)
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilter, null);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (nfcAdapter != null)
            nfcAdapter.disableForegroundDispatch(this);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        intento = intent;
        if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Toast.makeText(MainActivity.this, "NFC intent", Toast.LENGTH_LONG).show();
            Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (parcelables != null && parcelables.length > 0) {
                readTextFromTag((NdefMessage) parcelables[0]);
            }
        }
        if (!construction.isEmpty() || !fetchedData.isEmpty()) {
            al3b(construction);
        }
        Set<Integer> set = map.keySet();
        for (Integer a : set) {
            String key = map.get(a);
            String aKey = key.split(" ")[0] + key.split(" ")[1];

            Button btn = (Button) findViewById(getResources().getIdentifier("b" + aKey.trim(), "id", getPackageName()));
            Log.i("aaaaa", "b" + aKey.trim());

            if (btn != null && !btn.isClickable()) {
//                Log.i("bbbbb", "b" + aKey.trim());
                btn.setText("N/A");
                btn.setTextSize(11);
                btn.setTextColor(Color.RED);
            }
        }
        Log.w("NFCFr", "onNewIntent: " + set.toString());
    }


    private void writeNdefMessage(Tag tag, NdefMessage ndefMessage) {

        try {


            if (tag == null) {
                Toast.makeText(this, "Tag object cannot be null", Toast.LENGTH_SHORT).show();
                return;
            }

            Ndef ndef = Ndef.get(tag);

            if (ndef == null) {
                // format tag with the ndef format and writes the message.
                formatTag(tag, ndefMessage);
                Toast.makeText(this, "not ndef format", Toast.LENGTH_SHORT).show();

            } else {
                ndef.connect();

                if (!ndef.isWritable()) {
                    Toast.makeText(this, "Tag is not writable!", Toast.LENGTH_SHORT).show();
                    ndef.close();
                    return;
                }
                ndef.writeNdefMessage(ndefMessage);
                ndef.close();
                Toast.makeText(this, "Tag written!", Toast.LENGTH_SHORT).show();

            }

        } catch (Exception e) {
            Log.e("writeNdefMessage", e.getMessage());
        }
    }

    private NdefRecord createTextRecord(String content) {
        try {
            byte[] language;
            language = Locale.getDefault().getLanguage().getBytes("UTF-8");
            final byte[] text = content.getBytes("UTF-8");
            final int languageSize = language.length;
            final int textLength = text.length;
            final ByteArrayOutputStream payload = new ByteArrayOutputStream(1 + languageSize + textLength);
            payload.write((byte) (languageSize & 0x1F));
            payload.write(language, 0, languageSize);
            payload.write(text, 0, textLength);
            return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload.toByteArray());
        } catch (UnsupportedEncodingException e) {
            Log.e("createTextRecord", e.getMessage());
        }
        return null;
    }

    private boolean isWellFormatted(String cont) {
        String[] split = cont.split("@");
        Log.i("yallla2a ", split.length + "");
        if (split.length == 2 || split.length == 1)
            return true;

        if (cont.isEmpty())
            return false;
        else
            return true;
    }

    public void readTextFromTag(NdefMessage ndefMessage) {
        NdefRecord[] ndefRecords = ndefMessage.getRecords();
        if (ndefRecords != null && ndefRecords.length > 0) {
            NdefRecord ndefRecord = ndefRecords[0];
            String tagContent = getTextFromNdefRecord(ndefRecord).trim();
            tagContent = EncryptDecryptStringWithDES.performDecryption(tagContent);
            if (!message.isEmpty())
                tagContent = message;
            if (!isWellFormatted(tagContent)) {
                SharedPreferences cachedVersion = getSharedPreferences("cached_version", 0);
                tagContent = cachedVersion.getString("cached_version", "");
            }
            //SharedPreferences cachedVersion = getSharedPreferences("cached_version", 0);
            //String mTest=  cachedVersion.getString("cached_version", "");
            //Log.i("cachedddd", mTest);
            String[] ax = tagContent.split("@");
            ln = ax.length;
            if (construction.isEmpty() ) {
                construction = ax[0];
            }
            al3b(ax[0]);
            Log.w("SLOTS", "readTextFromTag: " + ax[0]);
            if (ln == 1) {
                tagContent = "";
            } else {
                tagContent = ax[1];
            }
            Log.i("habl", construction);
            Log.i("ConTag", tagContent);
            Log.i("ax    axxxxx", ax.length + "");
//            if (ln != 1) {
            //initFragment();
            deserialize(tagContent);
            for (int i = 0; i < fragments.length - 1; i++) {
                for (int j = 0; j < fragments[i].length - 1; j++) {
                    Log.i("<<<<", Arrays.toString(fragments[i][j]));
                }
            }
            for (int i = 0; i < fragments.length; i++) {
                for (int j = 0; j < fragments[i].length; j++) {
                    String ini = "";
                    Button m = (Button) findViewById(getResources().getIdentifier("b" + i + j, "id", getPackageName()));
                    for (int k = 0; k < fragments[i][j].length; k++) {
                        if (fragments[i][j][k] != null && fragments[i][j][k].length() != 0 && !fragments[i][j][k].equals("null")) {
                            Log.i("henaaaa  ", fragments[i][j][k].toString());
                            if (!fragments[i][j][k].toString().trim().equals("&"))
                                ini += getInitials(fragments[i][j][k].toString().split("&")[0]) + "|";
                        }
                    }
                    if (m != null) {
                        m.setText(ini);
                        m.setTextSize(11);
                    }
                }
            }
//            }
        } else {
            Toast.makeText(this, "No NDEF records found", Toast.LENGTH_LONG).show();
        }

    }

    public String getTextFromNdefRecord(NdefRecord ndefRecord) {
        String tagContent = null;
        try {
            byte[] payload = ndefRecord.getPayload();
            String textEncoding;
            if ((payload[0] & 128) == 0) textEncoding = "UTF-8";
            else textEncoding = "UTF-16";
            int languageSize = payload[0] & 0063;
            tagContent = new String(payload, languageSize + 1,
                    payload.length - languageSize - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("getTextFromNdefRecord", e.getMessage(), e);
        }
        return tagContent;
    }

    private NdefMessage createNdefMessage(String content) {
        NdefRecord ndefRecord = createTextRecord(content);
        NdefMessage ndefMessage = new NdefMessage(new NdefRecord[]{ndefRecord});
        return ndefMessage;
    }


    private void formatTag(Tag tag, NdefMessage ndefMessage) {
        try {
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);
            if (ndefFormatable == null) {
                Toast.makeText(this, "Tag is not ndef formatable!", Toast.LENGTH_SHORT).show();
                return;
            }
            ndefFormatable.connect();
            ndefFormatable.format(ndefMessage);
            ndefFormatable.close();
            Toast.makeText(this, "Tag writen!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("formatTag", e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String getInitials(String s) {
        String[] spInitials = s.split(" ");
        String initials = "";
        for (String s1 : spInitials)
            initials += s1.charAt(0);

        return initials;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override

        public void onClick(View v) {
            final Button x = (Button) findViewById(v.getId());
            ArrayList<String> timings = new ArrayList<String>();
            String readText = x.getText().toString();
            if (readText.split(",").length < 3) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Office hours form ");
//                alertDialog.setMessage("Office hours reservation");
                final Spinner spinner = new Spinner(MainActivity.this);
                String day_slot = map.get(x.getId());
                final int day = Integer.parseInt(day_slot.split(" ")[0]);
                final int slot = Integer.parseInt(day_slot.split(" ")[1]);
                if (fragments[day][slot][0] == null || fragments[day][slot][0].equals("null")) {
                    timings.add("1st");
                }
                if (fragments[day][slot][1] == null || fragments[day][slot][1].equals("null")) {
                    timings.add("2nd");
                }
                if (fragments[day][slot][2] == null || fragments[day][slot][2].equals("null")) {
                    timings.add("3rd");
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                        android.R.layout.simple_spinner_item, timings);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        Log.v("item", (String) parent.getItemAtPosition(position));
                        time = (String) parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                //ADDED NEW
                final TextView ay = new TextView(MainActivity.this);
                ay.setText("STUDENT NAME");
                final EditText nameInput = new EditText(MainActivity.this);
                final TextView ay2 = new TextView(MainActivity.this);
                ay2.setText("STUDENT ID");
                final EditText idInput = new EditText(MainActivity.this);
                final TextView ay3 = new TextView(MainActivity.this);
                ay3.setText("STUDENT EMAIL");
                final EditText emailInput = new EditText(MainActivity.this);
                //

                LinearLayout ret = new LinearLayout(MainActivity.this);
                final TextView ax = new TextView(MainActivity.this);
                ax.setText("Leave a message");
                //ax.setGravity(Gravity.CENTER);
                final EditText user_message = new EditText(MainActivity.this);

                //ADDED NEW
                nameInput.setText(userName);
                idInput.setText(userID);
                emailInput.setText(userEmail);
                //
                ret.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                ret.setOrientation(LinearLayout.VERTICAL);
                ret.addView(ay);
                ret.addView(nameInput);
                ret.addView(ay2);
                ret.addView(idInput);
                ret.addView(ay3);
                ret.addView(emailInput);
                ret.addView(ax);
                ret.addView(user_message);
                ret.addView(spinner);
                alertDialog.setView(ret);
                if (timings.size() != 0) {
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (time.equals("")) {
                                        error();
                                    }
//                                Toast.makeText(MainActivity.this, "Writing Message: " + time, Toast.LENGTH_LONG).show();
                                    int time_int;
                                    if (time.equals("1st"))
                                        time_int = 0;
                                    else if (time.equals("2nd"))
                                        time_int = 1;
                                    else time_int = 2;

                                    //ADDED NEW
                                    fragments[day][slot][time_int] = nameInput.getText().toString() + "&" + idInput.getText().toString()
                                            + "&" + emailInput.getText().toString() + "&" + user_message.getText().toString() +  "&" + checkInternet();
                                    //

                                    message = serialize();
                                    performWriteAction(getIntent(), message);

                                    Log.e("Message_", "onClick: " + message);
                                    performReadAction(getIntent());
                                    String [] startEndDates = MainActivity.generateSlotDateTime(day + "", slot + "", time_int + "");
                                    Log.e("START_END:", startEndDates[0] + " TO " + startEndDates[1]);
                                    googleCalenderData.add(emailInput.getText().toString());
                                    googleCalenderData.add(startEndDates[0]);
                                    googleCalenderData.add(startEndDates[1]);
                                    googleCalenderData.add(user_message.getText().toString() + " " + idInput.getText().toString());
                                    googleCalenderData.add(checkInternet());
                                    getResultsFromApi();
                                    dialog.cancel();
                                }
                            });
                }
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });

                alertDialog.show();
            } else {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setMessage("Slot is full");
                builder1.setCancelable(true);
                builder1.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        }

    };

    private void performWriteAction(Intent intent, String msg) {
        ln = 2;
        //  intento =  MainActivity.this.getIntent();
        try {
            if (intento != null) {
                Tag tag = intento.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                String newMsg = construction + "@" + msg;
                String encrypted = EncryptDecryptStringWithDES.performEncryption(newMsg);
                NdefMessage ndefMessage = createNdefMessage(encrypted);
                Log.i("!!!!!!!!!!!", newMsg);
                Log.i("zzzzzzzzz", tag + " a");
                writeNdefMessage(tag, ndefMessage);

                SharedPreferences cachedVersion = getSharedPreferences("cached_version", 0);
                SharedPreferences.Editor editor = cachedVersion.edit();
                editor.putString("cached_version", newMsg);
                editor.commit();

            } else {
                Toast.makeText(MainActivity.this, "Please tab the RFID tag", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
        }
    }

    void performReadAction(Intent intent) {
        if (intento.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Parcelable[] parcelables = intento.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (parcelables != null && parcelables.length > 0) {
                readTextFromTag((NdefMessage) parcelables[0]);
            }
        }
        Set<Integer> set = map.keySet();
        for (Integer a : set) {
            String key = map.get(a);
            String aKey = key.split(" ")[0] + key.split(" ")[1];

            Button btn = (Button) findViewById(getResources().getIdentifier("b" + aKey.trim(), "id", getPackageName()));
            // Log.i("aaaaa", "b" + aKey.trim());

            if (btn != null && !btn.isClickable()) {
                Log.i("bbbbb", "b" + aKey.trim());
                btn.setText("N/A");
                btn.setTextSize(11);
                btn.setTextColor(Color.RED);
            }
        }
    }

    private void error() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage("Please select a time slot");
        builder1.setCancelable(true);
        builder1.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private void getResultsFromApi() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (!isDeviceOnline()) {
            Toast.makeText(getApplicationContext(), "No network connection available.", Toast.LENGTH_SHORT).show();
        } else {
            new MakeRequestTask(mCredential).execute();
        }
    }

    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     *
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode  code indicating the result of the incoming
     *                    activity result.
     * @param data        Intent (containing result data) returned by incoming
     *                    activity result.
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    Toast.makeText(getApplicationContext(), "This app requires Google Play Services. Please install " +
                            "Google Play Services on your device and relaunch this app.", Toast.LENGTH_SHORT).show();
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;
        }
    }

    /**
     * Respond to requests for permissions at runtime for API 23 and above.
     *
     * @param requestCode  The request code passed in
     *                     requestPermissions(android.app.Activity, String, int, String[])
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either PERMISSION_GRANTED or PERMISSION_DENIED. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    /**
     * Callback for when a permission is granted using the EasyPermissions
     * library.
     *
     * @param requestCode The request code associated with the requested
     *                    permission
     * @param list        The requested permission list. Never null.
     */
    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }

    /**
     * Callback for when a permission is denied using the EasyPermissions
     * library.
     *
     * @param requestCode The request code associated with the requested
     *                    permission
     * @param list        The requested permission list. Never null.
     */
    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

    /**
     * Checks whether the device currently has a network connection.
     *
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     *                             Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                MainActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    /**
     * An asynchronous task that handles the Google Calendar API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
        private com.google.api.services.calendar.Calendar mService = null;
        private Exception mLastError = null;

        MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Google Calendar API Android Quickstart")
                    .build();
        }

        /**
         * Background task to call Google Calendar API.
         *
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of the next 10 events from the primary calendar.
         *
         * @return List of Strings describing returned events.
         * @throws IOException
         */
        public List<String> getDataFromApi() throws IOException {
            // List the next 10 events from the primary calendar.
            DateTime now = new DateTime(System.currentTimeMillis());
            List<String> eventStrings = new ArrayList<String>();
            Events events = mService.events().list("primary")
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();


            MainActivity.createEvent(mService, googleCalenderData.get(0), googleCalenderData.get(1), googleCalenderData.get(2), googleCalenderData.get(3), googleCalenderData.get(4));
            List<Event> items = events.getItems();

            for (Event event : items) {
                DateTime start = event.getStart().getDateTime();
                if (start == null) {
                    // All-day events don't have start times, so just use
                    // the start date.
                    start = event.getStart().getDate();
                }
                eventStrings.add(start.toString());
            }
            return eventStrings;
        }


        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @Override
        protected void onPostExecute(List<String> output) {
            mProgress.hide();
            Log.e("RES", "onPostExecute: " + output.toString());
//            List<String> dates = new ArrayList<>();
//            List<String> daysOfWeek = new ArrayList<>();
//            List<String> startTime = new ArrayList<>();
//            List<String> slots = new ArrayList<>();
//            List<Boolean> thisWeek = new ArrayList<>();
////            fetchedData = TextUtils.join("\n", output);
////            for (int i = 0; i < output.size(); i++) {
////                dates.add(extractDate(output.get(i)));
////                daysOfWeek.add(getDayWeek(dates.get(i)));
////                startTime.add(extractTime(output.get(i)));
////                slots.add(getSlot(startTime.get(i)));
////                thisWeek.add(inSameWeek(new Date(), parseDate(extractDate(output.get(i)))));
////            }
            //Log.e("API_", "onPostExecute: Dates " + dates.toString());
            //Log.e("API_", "onPostExecute: Days of the week " + daysOfWeek.toString());
            //Log.e("API_", "onPostExecute: Times " + startTime.toString());
            //Log.e("API_", "onPostExecute: Slots " + slots.toString());
            //Log.e("API_", "onPostExecute: Same week " + thisWeek.toString());
//            Toast.makeText(getApplicationContext(), "Dates " + dates.toString(),Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), "Days of the Week " + daysOfWeek.toString(),Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), "Start Times " + startTime.toString(),Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), "Slots " + slots.toString(),Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), "Same Week " + thisWeek.toString(),Toast.LENGTH_LONG).show();
//            for (int j = 0; j < slots.size(); j++) {
//                if (thisWeek.get(j)) {
//                    if (daysOfWeek.get(j) != "" && slots.get(j) != "") {
//                        fetchedDataDisplay += (daysOfWeek.get(j) + slots.get(j) + "-");
//                    }
//                }
//            }
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            MainActivity.REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(getApplicationContext(), "The following error occurred:\n"
                            + mLastError.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Request cancelled.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String extractDate(String dateTime) {
        int i;
        for (i = 0; i < dateTime.length(); i++) {
            if (dateTime.charAt(i) == 'T') {
                break;
            }
        }
        return dateTime.substring(0, i);
    }

    public String extractTime(String dateTime) {
        int i;
        for (i = 0; i < dateTime.length(); i++) {
            if (dateTime.charAt(i) == 'T') {
                break;
            }
        }
        i++;
        int j;
        for (j = 0; j < dateTime.substring(i).length(); j++) {
            if (dateTime.charAt(j) == '.') {
                break;
            }
        }
        j++;
        return dateTime.substring(i, j);
    }

    public static String getDayWeek() {
        Calendar c = Calendar.getInstance();
        //c.set(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)), Integer.parseInt(date.substring(8, 10)));
//        Log.e("TEST_",  (c.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) + " == " + Calendar.WEDNESDAY);

        return "" + c.get(Calendar.DAY_OF_WEEK) ;
//        int day = c.get(Calendar.DAY_OF_WEEK);
//        switch (day) {
//            case Calendar.SATURDAY:
//                return Integer.toString(0);
//            case Calendar.SUNDAY:
//                return Integer.toString(1);
//            case Calendar.MONDAY:
//                return Integer.toString(2);
//            case Calendar.TUESDAY:
//                return Integer.toString(3);
//            case Calendar.WEDNESDAY:
//                return Integer.toString(4);
//            case Calendar.THURSDAY:
//                return Integer.toString(5);
//
//        }
//        return "";
    }

    public String getSlot(String time) {
        LocalTime firstSlot = LocalTime.parse("08:00");
        LocalTime secondSlot = LocalTime.parse("10:29");
        LocalTime thirdSlot = LocalTime.parse("12:14");
        LocalTime forthSlot = LocalTime.parse("14:13");
        LocalTime fifthSlot = LocalTime.parse("15:59");
        LocalTime sixthSlot = LocalTime.parse("17:29");
        LocalTime t = LocalTime.parse(time);
        if (t.isAfter(firstSlot) && t.isBefore(secondSlot)) {
            return Integer.toString(0);
        }
        if (t.isAfter(secondSlot) && t.isBefore(thirdSlot)) {
            return Integer.toString(1);
        }
        if (t.isAfter(thirdSlot) && t.isBefore(forthSlot)) {
            return Integer.toString(2);
        }
        if (t.isAfter(forthSlot) && t.isBefore(fifthSlot)) {
            return Integer.toString(3);
        }
        if (t.isAfter(fifthSlot) && t.isBefore(sixthSlot)) {
            return Integer.toString(4);
        }
        return "";
    }

    public static boolean inSameWeek(Date date1, Date date2) {
        if (null == date1 || null == date2) {
            return false;
        }

        Calendar earlier = Calendar.getInstance();
        Calendar later = Calendar.getInstance();

        if (date1.before(date2)) {
            earlier.setTime(date1);
            later.setTime(date2);
        } else {
            earlier.setTime(date2);
            later.setTime(date1);
        }
        if (inSameYear(date1, date2)) {
            int week1 = earlier.get(Calendar.WEEK_OF_YEAR);
            int week2 = later.get(Calendar.WEEK_OF_YEAR);
            if (week1 == week2) {
                return true;
            }
        } else {
            int dayOfWeek = earlier.get(Calendar.DAY_OF_WEEK);
            earlier.add(Calendar.DATE, 7 - dayOfWeek);
            if (inSameYear(earlier.getTime(), later.getTime())) {
                int week1 = earlier.get(Calendar.WEEK_OF_YEAR);
                int week2 = later.get(Calendar.WEEK_OF_YEAR);
                if (week1 == week2) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean inSameYear(Date date1, Date date2) {
        if (null == date1 || null == date2) {
            return false;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        int year1 = cal1.get(Calendar.YEAR);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int year2 = cal2.get(Calendar.YEAR);
        if (year1 == year2)
            return true;

        return false;
    }

    public static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }


    /* Inserting an Event to Google API Calendar
     */
    public static void createEvent(com.google.api.services.calendar.Calendar service, String email, String startDate, String endDate, String Summary, String sent) {

//        HttpTransport transport = AndroidHttp.newCompatibleTransport();
//        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
//        com.google.api.services.calendar.Calendar service = new com.google.api.services.calendar.Calendar.Builder(
//                transport, jsonFactory, mCredential)
//                .setApplicationName("R_D_Location Callendar")
//                .build();
//
//
        Event event = new Event()
                .setSummary(Summary)
                .setLocation("GUC")
                .setDescription(Summary + " " + email);

        DateTime startDateTime = new DateTime(startDate);
        EventDateTime start = new EventDateTime()
                .setDateTime(startDateTime)
                .setTimeZone("Africa/Cairo");
        event.setStart(start);

        DateTime endDateTime = new DateTime(endDate);
        EventDateTime end = new EventDateTime()
                .setDateTime(endDateTime)
                .setTimeZone("Africa/Cairo");
        event.setEnd(end);

//        Event event = new Event()
//                .setSummary("Test Event")
//                .setLocation("Egypt")
//                .setDescription("Test Event");

//        DateTime startDateTime = new DateTime("2016-12-15T18:10:00+06:00");
//        EventDateTime start = new EventDateTime()
//                .setDateTime(startDateTime)
//                .setTimeZone("Africa/Egypt");
//        event.setStart(start);
//
//        DateTime endDateTime = new DateTime("2016-12-15T18:40:00+06:00");
//        EventDateTime end = new EventDateTime()
//                .setDateTime(endDateTime)
//                .setTimeZone("Africa/Egypt");
//        event.setEnd(end);

//        String[] recurrence = new String[]{"RRULE:FREQ=DAILY;COUNT=2"};
//        event.setRecurrence(Arrays.asList(recurrence));
        EventAttendee[] attendees;
        if(sent.equals("1")) {
            attendees  = new EventAttendee[]{
                    new EventAttendee().setEmail("jswilliam93@gmail.com") // Prof mail
            };
        }
        else {
            attendees = new EventAttendee[]{
                    new EventAttendee().setEmail("jswilliam93@gmail.com"), // Prof mail
                    new EventAttendee().setEmail(email)
            };
        }

        event.setAttendees(Arrays.asList(attendees));

        EventReminder[] reminderOverrides = new EventReminder[]{
                new EventReminder().setMethod("email").setMinutes(24 * 60),
                new EventReminder().setMethod("popup").setMinutes(10),
        };
        Event.Reminders reminders = new Event.Reminders()
                .setUseDefault(false)
                .setOverrides(Arrays.asList(reminderOverrides));
        event.setReminders(reminders);

        System.out.printf("Event created Hena: %s\n", event.getStart());
        String calendarId = "primary";

        try {
            event = service.events().insert("primary", event).setSendNotifications(true).execute();
            System.out.printf("Event created Check: %s\n", event);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.printf("Event created Exception: %s\n", e.getMessage());
        }
        System.out.printf("Event created: %s\n", event.getHtmlLink());
//
//        Event event = new Event();
//        event.setSummary("Event name here");
//        event.setLocation("event place here");
//
//        Date startDate = new Date();
//        Date endDate = new Date(startDate.getTime() + 3600000);
//        DateTime start = new DateTime(startDate, TimeZone.getTimeZone("UTC"));
//        event.setStart(new EventDateTime().setDateTime(start));
//        DateTime end = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
//        event.setEnd(new EventDateTime().setDateTime(end));
//        Event createdEvent;
//
//        try {
//            createdEvent = service.events().insert("primary", event).execute();
//            System.out.printf("Event created Check: %s\n", event);
//        } catch (IOException e) {
//            e.printStackTrace();
//            System.out.printf("Event created Exception: %s\n", e.getMessage());
//        }
//        System.out.println("Created event id: " + createdEvent.getId());
    }
//    private void writeToFile(String data, Context context) throws IOException {
//        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
//        outputStreamWriter.write(data);
//        outputStreamWriter.close();
//    }
//
//    public void sreviceAccountRequest() throws GeneralSecurityException, IOException {
//        System.out.println("hena");
//        writeToFile("test..", this);
//        System.out.println("hena");
//        JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
//        NetHttpTransport httpTransport = new com.google.api.client.http.javanet.NetHttpTransport();
//        GoogleCredential credential = new GoogleCredential.Builder()
//                .setTransport(httpTransport)
//                .setJsonFactory(JSON_FACTORY)
//                .setServiceAccountId("mayarali@rational-being-151310.iam.gserviceaccount.com")
//                .setServiceAccountPrivateKeyFromP12File(new File("app/src/main/assets", "key.p12"))
//                .setServiceAccountUser("jswilliam93@gmail.com")
//                .build();
//        com.google.api.services.calendar.Calendar service = new com.google.api.services.calendar.Calendar.Builder(httpTransport, JSON_FACTORY, credential)
//                .setApplicationName("Google Calendar API Android Quickstart")
//                .build();
//        Log.w("SERVICE_ACC", "sreviceAccountRequest: "+ service.calendarList().toString() );
//    }
    public static String [] generateSlotDateTime(String day, String slot, String slotPart)
    {
        //2016-12-17T18:10:00+06:00
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateandTime = sdf.format(new Date());

        String dayToday = MainActivity.getDayWeek();
        String dayRequested = MainActivity.calculateDay(Integer.parseInt(day) - Integer.parseInt(dayToday));

        String [] startEndTimes = MainActivity.calculateTime(slot, slotPart);
        String startTime = startEndTimes[0];
        String endTime = startEndTimes[1];
        String zoneTime = "+02:00";

        String [] startEndDates = new String[2];
        startEndDates [0] = dayRequested + "T" + startTime + zoneTime;
        startEndDates [1] = dayRequested + "T" + endTime + zoneTime;

        return startEndDates;
    }

    public static String calculateDay(int dayAdd) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        c.add(Calendar.DAY_OF_YEAR, dayAdd);
        return sdf.format(c.getTime());

    }

    public static String [] calculateTime(String slot, String slotPart) {
        int slotInt = Integer.parseInt(slot);
        int slotPartInt = Integer.parseInt(slotPart);
        String [] times = new String[2];
        switch (slotInt){
            case 0:
                switch (slotPartInt){
                    case 0:
                        times[0] = "08:30:00";
                        times[1] = "09:00:00";
                        return times;
                    case 1:
                        times[0] = "09:00:00";
                        times[1] = "09:30:00";
                        return times;

                    case 2:
                        times[0] = "09:30:00";
                        times[1] = "10:00:00";
                        return times;

                }
                break;

            case 1:
                switch (slotPartInt){
                    case 0:
                        times[0] = "10:30:00";
                        times[1] = "11:00:00";
                        return times;

                    case 1:
                        times[0] = "11:00:00";
                        times[1] = "11:30:00";
                        return times;

                    case 2:
                        times[0] = "11:30:00";
                        times[1] = "12:00:00";
                        return times;

                }
                break;

            case 2:
                switch (slotPartInt){
                    case 0:
                        times[0] = "12:15:00";
                        times[1] = "12:45:00";
                        return times;

                    case 1:
                        times[0] = "12:45:00";
                        times[1] = "13:15:00";
                        return times;

                    case 2:
                        times[0] = "13:15:00";
                        times[1] = "13:45:00";
                        return times;

                }
                break;

            case 3:
                switch (slotPartInt){
                    case 0:
                        times[0] = "14:15:00";
                        times[1] = "14:45:00";
                        return times;

                    case 1:
                        times[0] = "14:45:00";
                        times[1] = "15:15:00";
                        return times;

                    case 2:
                        times[0] = "15:15:00";
                        times[1] = "15:45:00";
                        return times;

                }
                break;

            case 4:
                switch (slotPartInt){
                    case 0:
                        times[0] = "16:00:00";
                        times[1] = "16:30:00";
                        return times;

                    case 1:
                        times[0] = "16:30:00";
                        times[1] = "17:00:00";
                        return times;

                    case 2:
                        times[0] = "17:00:00";
                        times[1] = "17:30:00";
                        return times;

                }

        }
        return times;
    }

    public String checkInternet() {
        return haveNetworkConnection()?"1":"0";
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


}
