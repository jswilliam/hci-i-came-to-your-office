package com.example.mayarelmohr.mpp.utilities;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by mimi on 12/3/15.
 */

public class Compresspor {
    public static byte[] compress(String str) throws Exception {
        if (str == null || str.length() == 0) {
            // return "".tob;
        }
        //System.out.println("String length : " + str.length());
        ByteArrayOutputStream obj = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(obj);
        gzip.write(str.getBytes("UTF-8"));
        gzip.close();
        String outStr = obj.toString("UTF-8");
        //System.out.println("Output String length : " + outStr.length());
        return obj.toByteArray();
    }

    public static String decompress(byte[] bytes) throws Exception {
        if (bytes == null || bytes.length == 0) {
            return bytes.toString();
        }
        //System.out.println("Input String length : " + bytes.length);
        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(
                bytes));

        BufferedReader bf = new BufferedReader(new InputStreamReader(gis,
                "UTF-8"));
        String outStr = "";
        String line;
        while ((line = bf.readLine()) != null) {
            outStr += line;
        }
        //System.out.println("Output String lenght : " + outStr.length());
        return outStr;
    }


}
