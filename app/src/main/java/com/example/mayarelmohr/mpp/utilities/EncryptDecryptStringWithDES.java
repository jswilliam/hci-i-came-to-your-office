package com.example.mayarelmohr.mpp.utilities;

/**
 * Created by mimi on 12/16/15.
 */
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;
import javax.crypto.spec.SecretKeySpec;

public class EncryptDecryptStringWithDES {

    private static Cipher ecipher;
    private static Cipher dcipher;
    private static String keyString = "I1G5neojeRo=";


    private static SecretKey key;

    public static void main(String[] args) {

        try {

            // generate secret key using DES algorithm
            key = KeyGenerator.getInstance("DES").generateKey();

            ecipher = Cipher.getInstance("DES");
            dcipher = Cipher.getInstance("DES");

            // initialize the ciphers with the given key

            ecipher.init(Cipher.ENCRYPT_MODE, key);

            dcipher.init(Cipher.DECRYPT_MODE, key);

            String encrypted = encrypt("mimi!");

            String decrypted = decrypt(encrypted);

            System.out.println("Decrypted: " + decrypted);

        } catch (NoSuchAlgorithmException e) {
            System.out.println("No Such Algorithm:" + e.getMessage());
            return;
        } catch (NoSuchPaddingException e) {
            System.out.println("No Such Padding:" + e.getMessage());
            return;
        } catch (InvalidKeyException e) {
            System.out.println("Invalid Key:" + e.getMessage());
            return;
        }

    }

    public static String performDecryption(String encrypted){
        try {
            key = new SecretKeySpec(
                    BASE64DecoderStream.decode(keyString.getBytes()), "DES");

            dcipher = Cipher.getInstance("DES");
            dcipher.init(Cipher.DECRYPT_MODE, key);
            String decrypted = decrypt(encrypted);
            return decrypted;
        } catch (Exception e) {
            System.out.println("No Such Algorithm:" + e.getMessage());
            return null;
        }

    }
    public static String performEncryption(String m){
        try {
            // generate secret key using DES algorithm
//      System.out.println(BASE64DecoderStream.decode(keyString.getBytes()));
            key = new SecretKeySpec(
                    BASE64DecoderStream.decode(keyString.getBytes()), "DES");
            ecipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            String encrypted = encrypt(m);
            return encrypted;
        } catch (Exception e) {
            System.out.println("No Such Algorithm:" + e.getMessage());
            return null;
        }

    }

    public static String encrypt(String str) {

        try {

            // encode the string into a sequence of bytes using the named
            // charset

            // storing the result into a new byte array.

            byte[] utf8 = str.getBytes("UTF8");

            byte[] enc = ecipher.doFinal(utf8);

            // encode to base64

            enc = BASE64EncoderStream.encode(enc);

            return new String(enc);

        }

        catch (Exception e) {

            e.printStackTrace();

        }

        return null;

    }

    public static String decrypt(String str) {

        try {

            // decode with base64 to get bytes

            byte[] dec = BASE64DecoderStream.decode(str.getBytes());

            byte[] utf8 = dcipher.doFinal(dec);

            // create new string based on the specified charset

            return new String(utf8, "UTF8");

        }

        catch (Exception e) {

            e.printStackTrace();

        }

        return null;

    }

}